<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh">
<head>
  <meta charset="UTF-8">
  <title>游戏</title>
</head>
<body>
<h1 align="center">游戏试玩(${userid})</h1>
<div><img src="http://tu.tingclass.net/uploads/2014/0321/20140321053133778.jpg" style="width:100%; overflow:hidden"/></div>

<div style="text-align:center;"><button onclick="pay(1);">直接支付1分</button></div>
<div style="text-align:center;"><button onclick="pay(10);">直接支付10分</button></div>
<div style="text-align:center;"><button onclick="pay(100);">直接支付1元</button></div>
<div style="text-align:center;"><button onclick="pay(500);">直接支付5元</button></div>
<div style="text-align:center;"><button onclick="pay(1100);">直接支付11元</button></div>
<div style="text-align:center;"><button onclick="pay(2300);">直接支付23元</button></div>
<div style="text-align:center;"><button onclick="pay(5000);">直接支付50元</button></div>
<div style="text-align:center;"><button onclick="pay(31);">直接支付31分</button></div>
<div style="text-align:center;"><button onclick="pay(100000);">直接支付1000分</button></div>
<div style="text-align:center;"><button onclick="subscribe();">关注公众号</button></div>
<div style="text-align:center;"><button onclick="refresh();">刷新游戏</button></div>
<script src="http://lib.sinaapp.com/js/zepto/1.0/zepto.min.js" ></script>
<script src="http://game.lieqicun.cn/static/js/top_game_sdk.min.js"></script>
<script>
    TPGAME_SDK.config({
        share: {
            friend: {title: 'share to friend', 'desc': 'hello world', success: function(){alert('share success');}},
            timeline: {title: 'share to cricle', 'imgUrl' : 'http://tu.tingclass.net/uploads/2014/0321/20140321053133778.jpg', cancel: function(){alert('share timeline cancel');}}
        },
        pay : {
            success: function(){alert('支付成功');},
            cancel: function(){alert('支付已取消');}
        }
    });

    function pay(fee){
        $.ajax({
            url: 'order.json',
            data: {fee:fee},
            type: 'POST',
            dataType: 'json',
            success: function(result){
                alert(JSON.stringify(result));

                TPGAME_SDK.pay(result);
            },
            error: function(){
                alert('prepay error');
            }
        });
    }

    function subscribe() {
        TPGAME_SDK.showQRCode();
    }

    function refresh() {
        location.href="";
    }
</script>
</body>
</html>
