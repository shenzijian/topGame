package tpgame.util;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Arrays;

/**
 * SHA1签名工具类
 */
public class SHA1 {

    /**
     * 串接arr参数，生成sha1 digest
     * @param arr
     * @return
     */
    public static String gen(String... arr) {
        Arrays.sort(arr);
        StringBuilder sb = new StringBuilder();
        for (String a : arr) {
            sb.append(a);
        }
        try {
            return DigestUtils.sha1Hex(sb.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
