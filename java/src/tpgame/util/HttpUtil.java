package tpgame.util;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

public class HttpUtil {
	
	private static Logger logger = LoggerFactory.getLogger("webcall");
	
	private static final String default_charaset="utf-8";
	
	private static final int default_connect_timeout = 180 * 1000;
	private static final int default_socket_timeout = 60 * 1000;
	private static final int default_retries = 1;
	
	private static HttpClient makeHttpClient(int socketTimeout, int connectTimeout, int retries, String charset){
		HttpClient client = new HttpClient();
        HttpClientParams clientParams = client.getParams();
        clientParams.setParameter("http.socket.timeout", socketTimeout); 
        clientParams.setParameter("http.connection.timeout", connectTimeout); 
        // connection建立超时
        clientParams.setParameter("http.connection-manager.timeout", new Long(socketTimeout));
        clientParams.setParameter("http.method.retry-handler",
                new DefaultHttpMethodRetryHandler(retries, false)); // 如果Http出错，三次重试
        
        clientParams.setContentCharset(charset);//设置请求编码
        return client;
	}

	public static String postByForm(String url, Map<String,String> params,int timeOut) throws Exception {
		return postByForm(url, params, timeOut, timeOut, default_retries, default_charaset);
	}
	public static String postByForm(String url, Map<String,String> params) throws Exception {
        return postByForm(url, params, default_connect_timeout, default_socket_timeout, default_retries, default_charaset);
    }
	public static String postByForm(String url, Map<String,String> params, int connectTimeout, int socketTimeout, int retries, String charset) throws Exception {
		HttpClient client = makeHttpClient(connectTimeout, socketTimeout, retries, charset);
		PostMethod post = new PostMethod(url);
		String jsonStr = "";
		InputStream ins = null;
		long start = 0L;
		try {
			post.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset="+charset);

			if(params!=null&&!params.isEmpty()){
				NameValuePair[] paramsPair = new NameValuePair[params.size()];
				Iterator<Map.Entry<String,String>> iter = params.entrySet().iterator();
				int i=0;
				while (iter.hasNext()) {
					Map.Entry<String,String> entry = iter.next();
					NameValuePair jsonArry = new NameValuePair(entry.getKey(),entry.getValue());
					paramsPair[i] = jsonArry;
					i = i+1;
				}
				post.setRequestBody(paramsPair);
			}
			if(logger.isInfoEnabled())
				logger.info("HttpUtil: post " + params);
			start = System.currentTimeMillis();
			int status = client.executeMethod(post);
			if(status == 200){
				jsonStr = post.getResponseBodyAsString();
			} else {
				logger.error("HttpUtil.postByForm error, url={}, response code:{}, headers:{}",
						url, status, Arrays.toString(post.getResponseHeaders()));
				post.abort();
			}
		} catch (Exception e) {
			if(logger.isErrorEnabled())
				logger.error("HttpUtil.simplePost error, url="+url, e);
			throw e;
		} finally {
			if (null != ins) {
				try {
					ins.close();
				} catch (IOException e) {
				}
			}
			long time = System.currentTimeMillis()-start;
			if(logger.isInfoEnabled())
				logger.info("调用接口"+url+"耗时："+time);
			if(null != post){
				post.releaseConnection();
			}
		}
		return jsonStr;
	}

}
