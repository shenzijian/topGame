package tpgame.util;

/**
 * 对接参数
 */
public class Constants {

    public static final String partnerid = "4d4bb591c64c912e007360184faaa743";
    public static final String secret = "4f07683af6f85cbf6d82b3edbb2536da";

    //获取用户信息
    public static final String userInfoApi = "http://api.99kgames.com/vendor/api/userinfo";

    //检测用户是否关注公众号
    public static final String subCheckApi = "http://api.99kgames.com/vendor/api/subscribe";

}
