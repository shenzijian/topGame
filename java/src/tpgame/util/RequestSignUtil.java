package tpgame.util;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 请求签名工具类
 */
public class RequestSignUtil {

	/**
	 * 根据请求数据生成排序的请求字符串
	 * @param params
	 * @return
	 */
	public static String getSortedRequestString(Map<String, String> params){
		StringBuilder sb = new StringBuilder();
		TreeMap<String, String> treeMap = new TreeMap<String,String>(params);
		for(Entry<String, String> entry : treeMap.entrySet()){
			String value = entry.getValue();
			if (value == null){
				value = "";
			}
			sb.append(entry.getKey()).append('=').append(value).append('&');
		}
		
		//删除末尾的&
		if(sb.length() > 0){
			sb.setLength(sb.length() - 1);
		}
		
		return sb.toString();
	}

	/**
	 * 获取http 请求中的参数及值的map
	 * @param request
	 * @return
	 */
	public static Map<String, String> getRequestParams(HttpServletRequest request){
		Map<String,String> params = new HashMap<>();
		Map requestParams = request.getParameterMap();

		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";

			//同名的值用逗号分隔
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}

		return params;
	}

}
