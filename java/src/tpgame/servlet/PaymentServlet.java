package tpgame.servlet;

import net.sf.json.JSONObject;
import tpgame.util.Constants;
import tpgame.util.RequestSignUtil;
import tpgame.util.SHA1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Description 订单支付
 *
 * @ClassName PaymentServlet
 *
 * @Copyright 北京创力聚点
 *
 * @Project TP_GAME
 *
 * @Author bkning
 *
 * @Create Date 2017-08-01
 *
 */
@WebServlet(name = "order.json")
public class PaymentServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String money = request.getParameter("fee"); //以"分"为单位

        Map<String, String> params = new HashMap<>();
        params.put("total_fee", money);
        params.put("out_trade_no", "" + System.currentTimeMillis());
        params.put("body", "直接购买");
        params.put("product_id", "" + 123);
        params.put("detail", "");
        params.put("attach", "");

        String reqStr = RequestSignUtil.getSortedRequestString(params);
        String sign = SHA1.gen(reqStr + Constants.secret);
        params.put("sign", sign);

        JSONObject jsonObject = JSONObject.fromObject(params);
        jsonObject.put("code", 0);
        jsonObject.put("message", "success");

        response.setContentType("text/json; charset=utf-8");
        PrintWriter rw = response.getWriter();
        rw.append(jsonObject.toString());
        rw.flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
