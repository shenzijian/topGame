package tpgame.servlet;

import net.sf.json.JSONObject;
import tpgame.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Description 游戏登录
 *
 * @ClassName LoginServlet
 *
 * @Copyright 北京创力聚点
 *
 * @Project TP_GAME
 *
 * @Author bkning
 *
 * @Create Date 2017-08-01
 *
 */
@WebServlet(name = "login.html")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accessToken = request.getParameter("access_token");

        String result = getUserInfo(accessToken);
        try {
            JSONObject json = JSONObject.fromObject(result);
            request.setAttribute("userid", json.get("userid"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    /**
     * 获取用户信息
     * @param accessToken
     */
    private String getUserInfo(String accessToken) {
        Map<String, String> params = new HashMap<>();
        params.put("partnerid", Constants.partnerid);
        params.put("access_token", accessToken);

        String reqStr = RequestSignUtil.getSortedRequestString(params);
        String sign = SHA1.gen(reqStr + Constants.secret);
        params.put("sign", sign);

        try {
            return HttpUtil.postByForm(Constants.userInfoApi, params);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "{}";
    }
}
