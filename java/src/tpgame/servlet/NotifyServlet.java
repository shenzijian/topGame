package tpgame.servlet;

import org.apache.commons.lang.StringUtils;
import tpgame.util.Constants;
import tpgame.util.RequestSignUtil;
import tpgame.util.SHA1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Description 支付成功通知回调
 *
 * @ClassName NotifyServlet
 *
 * @Copyright 北京创力聚点
 *
 * @Project TP_GAME
 *
 * @Author bkning
 *
 * @Create Date 2017-08-01
 *
 */
@WebServlet(name = "notify.do")
public class NotifyServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> reqParams = RequestSignUtil.getRequestParams(request);
        String sign = reqParams.get("sign");

        if(StringUtils.isNotBlank(sign)) {
            reqParams.remove("sign");

            String reqStr = RequestSignUtil.getSortedRequestString(reqParams);
            String newSign = SHA1.gen(reqStr + Constants.secret);

            if(sign.equals(newSign)){
                PrintWriter rw = response.getWriter();
                rw.append("success");
                rw.flush();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
