<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Demo</title>
</head>
<body>

<?php
/**
 *
 *
 * 游戏金榜-demo
 * 	@author shenzijian 2017-01-03
 * 获得用户信息--示例
 * 保存用户信息到session
 */
require_once('topGame.class.php');
$topGame = new topGame();
$userInfo = $topGame->getUserInfo();
$_SESSION['userid']=$userInfo->userid;
echo '<pre>';print_r($userInfo);
?>
<script typet="text/javascript" src="http://lib.sinaapp.com/js/jquery/1.9.1/jquery-1.9.1.min.js"></script>
<script typet="text/javascript" src="https://cdn.99kgames.com/js/tpgame_sdk.min.js"></script>
<input type="button" id="check" value="检测用户是否关注微信公众号示例" />
<input type="button" id="pay" value="调用支付示例" />
<input type="button" id="show" value="关注二维码弹框" />

<script>
    //游戏分享配置--直接复制
    params= {
        share: {
            friend: {
                title: '分享给好友的标题',
                desc: '分享给好友的描述',
                imgUrl: '分享给好友时的图标',
                success: function(){/*分享好友成功回调*/},
                cancel: function(){/*分享好友取消的回调*/},
            },
            timeline: {
                title: '分享朋友圈的标题',
                imgUrl: '分享朋友圈的图片链接',
                success: function(){/*分享朋友圈成功回调*/},
                cancel: function(){/*分享朋友圈取消或失败回调*/},
            }
        },
        pay: {
            success: function(){/*支付成功回调*/
            },
            cancel: function(){/*支付失败回调*/},
        }
    };

    TPGAME_SDK.config(params);

    //关注二维码--直接复制
    $('#show').click(function () {
        TPGAME_SDK.showQRCode();
    })

    //调用支付
    $('#pay').click(function () {
        out_trade_no = 123456;//厂商订单编号不能重复
        product_id = 111;//商品id
        total_fee = 1;//'支付总金额，以分为单位，必须大于0'
        body = "名称";//'订单或商品的名称'
        detail = "详情";//'订单或商品的详情'
        attach = "附加数据";//'附加数据，后台通知时原样返回'
        $.post('pay.php',{out_trade_no:out_trade_no,product_id:product_id,total_fee:total_fee,body:body,detail:detail,attach:attach},function(result){
            TPGAME_SDK.pay(result);
        },'json');

    });

    //检查是否关注公众号
    $('#check').click(function () {
        $.get('checkSub.php',function(result){
            alert(result);
        },'html');

    });
</script>
</body>
</html>