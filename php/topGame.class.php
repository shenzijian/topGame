<?php
/**
 *游戏金榜-demo
 *@author shenzijian 2017-01-03
 * config.php为配置文件,也可以把配置文件的信息写入当前文件,去掉配置文件
 */
require_once('config.php');

class topGame {
     
    private $appkey ;
 
    private $partnerid ;
    
    private $userInfoUrl = 'http://api.99kgames.com/vendor/api/userinfo';
    
    private $checkSubUrl = 'http://api.99kgames.com/vendor/api/subscribe';

    /**
     * 初始化
     * @param  [string] $partnerid   [厂商id]
     * @param  [string] $appkey  [签名密钥]
     */
    public function __construct(){
        $this->partnerid = PARTNERID;
        $this->appkey = APPKEY;
    }
    
     /**
     * 获得用户信息
     * @return  [obj]
     */
    public function getUserInfo(){
        $params = array(
            'access_token' => $_GET['access_token'],
            'partnerid'   => $this->partnerid
        );
        $url=$this->get_sign_url($this->userInfoUrl,$params);
        return $this->curl($url);
    }
    
     /**
     * 检测用户是否关注公众号
     * @return  [obj]
     */
    public function checkSub($user_identify){
         $params = array(
            'partnerid'   => $this->partnerid,
            'user_identify' => $user_identify,

        );
        $url=$this->get_sign_url($this->checkSubUrl,$params);
        return $this->curl($url);
    }
    
    /**
     * 生成支付签名,返回支付信息
     * @return  [array]
     */
    public function getPayInfo(){
        $params=array();
        foreach ($_POST as $key=>$value){
            $params[$key]=($value?$value:"");
        }
        ksort($params);
        $str='';
        foreach($params as $key=>$value){
            $str.=$key.'='.$value.'&';
        }
        $str=rtrim($str, "&");
        $params['sign']=sha1($str.$this->appkey);
        echo json_encode($params);
    }
    
    /**
     * 支付成功后台通知接口签名校验
     * @return  [boolean]
     */
    public function checkCallBack(){
        $sign=$_POST['sign'];
        unset($_POST['sign']);
        $params=array();
        foreach ($_POST as $key=>$value){
            if($key!='sign'){
                $params[$key]=($value?$value:"");
            }
        }
        ksort($params);
        $str='';
        foreach($params as $key=>$value){
            $str.=$key.'='.$value.'&';
        }
        $str=rtrim($str, "&");
        $params['sign']=sha1($str.$this->appkey);
        if($sign == $params['sign']){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 获得用户信息请求url地址
     * @return  [string]
     */
    private function get_sign_url($url,$params){
        ksort($params);
        $str='';
        foreach($params as $key=>$value){
            $str.=$key.'='.$value.'&';
        }
        $str=rtrim($str, "&");
        $sign=sha1($str.$this->appkey);
        $url.='?'.$str.'&sign='.$sign;
        return $url;
    }
    
    /**
     * 请求接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $params [请求的参数]
     * @param  int $ipost [是否采用POST形式]
     * @return  string
     */
    private function curl($url,$params=false,$ispost=0){
        $httpInfo = array();
        $ch = curl_init();
 
        curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
        curl_setopt( $ch, CURLOPT_USERAGENT , 'JuheData' );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
        curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
        if( $ispost )
        {
            curl_setopt( $ch , CURLOPT_POST , true );
            curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
            curl_setopt( $ch , CURLOPT_URL , $url );
        }
        else
        {
            if($params){
                curl_setopt( $ch , CURLOPT_URL , $url.'?'.$params );
            }else{
                curl_setopt( $ch , CURLOPT_URL , $url);
            }
        }
        $response = curl_exec( $ch );
        if ($response === FALSE) {
            echo "cURL Error: " . curl_error($ch);
        }
        $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
        $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
        curl_close( $ch );
        return json_decode($response);
    }
    
}