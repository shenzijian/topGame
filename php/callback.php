<?php
/**
 *支付回调demo
 *@author shenzijian 2017-01-03
 * 验证签名-通知游戏金榜接收成功-保存订单信息
 * 返回数据说明 厂商收到支付成功通知后，需要在3分钟内处理完成，返回字符串success表示处理成功。超过3分钟，返回值不为success或者http状态码错误都表示处理失败。
 * 当通知失败时，游戏金榜会重试向厂商的支付通知地址发通知，直到通知成功为止。重试通知时，除timestamp和sign参数以外，其他参数都保持不变。通知的频率如下：
 * 支付成功1分钟内：每10秒钟通知一次 支付成功10分钟内：每1分钟通知一次 支付成功1小时内：每10分钟通知一次 支付成功超过1小时：每1小时通知一次
 */

file_put_contents('topGame.txt',json_encode($_POST)."\n",FILE_APPEND);

require_once('topGame.class.php');

$topGame = new topGame();

if ($topGame->checkCallBack()) {
    echo 'success';
    $params=array();
    $params['transaction_id'] = $_POST['transaction_id'] ;//交易编号	该笔支付在游戏金榜的交易单号，每笔支付唯一	56707930415089408
    $params['game'] = $_POST['game'];//game	游戏代号	游戏的唯一标识，提前约定好的代号	chuanqi
    $params['out_trade_no'] = $_POST['out_trade_no'];//	厂商订单编号	厂商发起支付时填写的订单编号	2016040118726
    $params['trade_status'] = $_POST['trade_status'];//	支付状态	目前固定为SUCCESS，表示支付成功	SUCCESS
    $params['total_fee'] = $_POST['total_fee'];//总金额	订单总金额，以分为单位	100
    $params['product_id'] = $_POST['product_id'];//product_id	厂商商品编号	厂商发起支付时填写的商品id	574
    $params['partnerid'] = $_POST['partnerid'];//厂商编号	厂商唯一标识，由游戏金榜提供	xyzhudong
    $params['attach'] = $_POST['attach'];//商附加数据	厂商发起支付时填写的附加数据
    $params['userid'] = $_POST['userid'];//用户标识	用户唯一标识，一般为8位数字	63473607
    $params['pay_time'] = $_POST['pay_time'];//支付时间	用户支付成功的时间,“年月日时分秒”不带空格的格式：20160401112007	20160401112007
    $params['timestamp'] = $_POST['timestamp'];//通知时间	发起支付通知的时间，以秒为单位	1450187745
} else {
    echo '签名未校验通过';
}